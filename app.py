from flask import Flask
from flask_restful import Api
import os
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.config['UPLOAD_FOLDER'] = "{}/uploads".format(os.getcwd())

api = Api(app)

#------- JWT -------
from flask_jwt_extended import JWTManager
from flask_jwt_extended import create_access_token
from flask_jwt_extended import get_jwt_identity
from flask_jwt_extended import jwt_required

app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
jwt = JWTManager(app)

from resource import Hello, UserRegistration, UserLogin

api.add_resource(Hello, '/home')
api.add_resource(UserRegistration, '/employee-register')
api.add_resource(UserLogin, '/employee-login')
# api.add_resource(resource.UserRegistration, '/company-register')
# api.add_resource(resource.UserLogin, '/employee-login')


if  __name__ == '__main__':
     app.run(port=9000,debug=True)