import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from random import *
from flask import Flask
from datetime import datetime
basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:secretpass@127.0.0.1:5432/aphelion"
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


# app.config["MONGOALCHEMY_DATABASE"] = "Backend_Python"
# app.config['MONGOALCHEMY_SERVER_AUtH'] = False
# db = MongoAlchemy(app)


class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    employee_id = db.Column(db.String())
    employee_name = db.Column(db.String(),unique=True,nullable=False)
    password = db.Column(db.String())
    email_id = db.Column(db.String())
    contact_number = db.Column(db.String())
    designation = db.Column(db.String())
    created_date = db.Column(db.DateTime(), default=datetime.now())
    

    def __init__(self, employee_id, employee_name, password, email_id, contact_number, designation):
        self.employee_id = employee_id
        self.employee_name = employee_name
        self.password = password
        self.email_id = email_id
        self.contact_number = contact_number
        self.designation = designation

    def __repr__(self):
        return '<id {}>'.format(self.id)