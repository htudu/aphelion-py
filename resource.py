from flask_restful import Resource, Api, request
import uuid
import random
from random import *
import datetime
from models.models import db, Users
from sqlalchemy import or_
from werkzeug.utils import secure_filename
import os
from flask_jwt_extended import create_access_token

class Database():

    def __init__(self):
        pass

    def row2dict(self,row):
        d = {}
        for column in row.__table__.columns:
            d[column.name] = str(getattr(row, column.name))
        return d


class Hello(Resource):
    def get(self):
        return "Hello! Welcome to Recipe Book!!"

class UserRegistration(Resource):
    def get(self):
        return { "message": "User with get nethod allowed!!",
                "code": 200}

    def post(self):
        payload = request.get_json()
        #--- next add to database ---

        rec = Users(**payload)
        db.session.add(rec)
        db.session.commit()
        #--------
        return { "message": "User has been registered!!",
                "code": 200}

class UserLogin(Resource):
    def post(self):
        data = request.get_json()

        rec = db.session.query(Users).filter(Users.email == data["employee_id"]).first()
        if rec:
            pwd= rec.password
            if pwd == data["password"]:
                additional_claims = {"role": "employee", 
                                    "company": "TimeTok", 
                                    "urls":["/employee-login","/employee-register"]
                                    }
                access_token = create_access_token(identity=data["employee_id"], additional_claims = additional_claims)
                return { "message": "Welcome Back!",
                         "code": 200}

            else : 
                return { "message": "Incorrect password!!!",
                         "code": 400}
        else :
            return { "message": "User not found", "code": 400}

class FaceLogin(Resource):
    def post(self):
        data = request.get_json()
        print("login data====",data)
        # TODO : read image data
        # TODO : detect face
        # TODO : extract face encoding
        # TODO : compare face encoding
        # TODO : if employee -> log time
        # TODO : if employee break/logout -> log time
        return True

class ForgotPass(Resource):
    def post(self):
        data = request.get_json()
        print("password====", data)

        rec = db.session.query(Users).filter(Users.email == data["email"]).first()
        if rec:
            rec.password = data["new_password"]
            db.session.commit()
            return { "message": "Password updated succesfully!!"}
        else:
            return { "message": "User doesnot exist"}